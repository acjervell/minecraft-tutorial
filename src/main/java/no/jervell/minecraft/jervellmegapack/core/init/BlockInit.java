package no.jervell.minecraft.jervellmegapack.core.init;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.item.BlockItem;
import net.minecraftforge.common.ToolType;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import no.jervell.minecraft.jervellmegapack.JervellMegaPackMod;
import no.jervell.minecraft.jervellmegapack.core.blocks.CustomBlock;
import no.jervell.minecraft.jervellmegapack.core.blocks.TomatoCropsBlock;

public class BlockInit {
    public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, JervellMegaPackMod.MOD_ID);

    public static final RegistryObject<Block> ARNEBLOKK = BLOCKS.register(
            "arneblokk", () -> new Block(AbstractBlock.Properties
                    .create(Material.WOOD)
                    .harvestTool(ToolType.AXE)
                    .hardnessAndResistance(2)
            )
    );

    public static final RegistryObject<Block> TOMATOES = BLOCKS.register(
            "tomatoes", () -> new TomatoCropsBlock(AbstractBlock.Properties
                    .create(Material.PLANTS)
                    .harvestTool(ToolType.HOE)
                    .hardnessAndResistance(0)
                    .doesNotBlockMovement()
                    .tickRandomly()
                    .sound(SoundType.PLANT)
            )
    );
    public static final RegistryObject<CustomBlock> PIZZAOVEN = BLOCKS.register(
            "pizzaoven", () -> new CustomBlock(AbstractBlock.Properties
                    .create(Material.ROCK)
                    .harvestTool(ToolType.PICKAXE)
                    .hardnessAndResistance(3)
            )
    );
}
