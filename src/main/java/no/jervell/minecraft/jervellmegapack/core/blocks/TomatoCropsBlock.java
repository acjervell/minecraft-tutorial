package no.jervell.minecraft.jervellmegapack.core.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.CropsBlock;
import net.minecraft.util.IItemProvider;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import no.jervell.minecraft.jervellmegapack.core.init.ItemInit;

public class TomatoCropsBlock extends CropsBlock {

    private static final VoxelShape[] SHAPES = new VoxelShape[] {
        Block.makeCuboidShape(0., 0., 0., 16., 2., 16. ),
        Block.makeCuboidShape(0., 0., 0., 16., 3., 16. ),
        Block.makeCuboidShape(0., 0., 0., 16., 4., 16. ),
        Block.makeCuboidShape(0., 0., 0., 16., 5., 16. ),
        Block.makeCuboidShape(0., 0., 0., 16., 6., 16. ),
        Block.makeCuboidShape(0., 0., 0., 16., 7., 16. ),
        Block.makeCuboidShape(0., 0., 0., 16., 8., 16. ),
        Block.makeCuboidShape(0., 0., 0., 16., 9., 16. )
    };

    public TomatoCropsBlock(Properties builder) {
        super(builder);
    }

    @Override
    protected IItemProvider getSeedsItem() {
        return ItemInit.TOMATO.get();
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        return SHAPES[state.get(getAgeProperty())];
    }
}
