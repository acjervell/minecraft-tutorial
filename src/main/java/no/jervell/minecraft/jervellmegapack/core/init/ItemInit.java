package no.jervell.minecraft.jervellmegapack.core.init;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.CropsBlock;
import net.minecraft.block.FurnaceBlock;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Food;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraftforge.common.ToolType;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import no.jervell.minecraft.jervellmegapack.JervellMegaPackMod;

public class ItemInit {
    public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, JervellMegaPackMod.MOD_ID);

    public static final RegistryObject<Item> PIZZA = ITEMS.register(
            "pizza", () -> new Item(new Item.Properties()
                    .group(ItemGroup.FOOD)
                    .food((new Food.Builder())
                            .hunger(6)
                            .saturation(1.2F)
                            .build()
                    )
            )
    );

    public static final RegistryObject<Item> ARNEBLOKK_ITEM = ITEMS.register(
            "arneblokk", () -> new BlockItem(BlockInit.ARNEBLOKK.get(), new Item.Properties()
                    .group(ItemGroup.BUILDING_BLOCKS)
            )
    );

    public static final RegistryObject<Item> TOMATO = ITEMS.register(
            "tomato", () -> new BlockItem(BlockInit.TOMATOES.get(), new Item.Properties()
                    .group(ItemGroup.FOOD)
                    .food((new Food.Builder())
                            .hunger(3)
                            .saturation(0.2F)
                            .build()
                    )
            )
    );
    public static final RegistryObject<Item> CHEESE = ITEMS.register(
            "cheese", () -> new Item(new Item.Properties()
                    .group(ItemGroup.FOOD)
                    .food((new Food.Builder())
                            .hunger(2)
                            .saturation(0.2F)
                            .build()
                    )
            )
    );
    public static final RegistryObject<Item> PIZZAOVEN_ITEM = ITEMS.register(
            "pizzaoven", () -> new BlockItem(BlockInit.PIZZAOVEN.get(), new Item.Properties()
                    .group(ItemGroup.DECORATIONS)
            )
    );
    public static final RegistryObject<Item> DOUGH = ITEMS.register(
            "dough", () -> new Item(new Item.Properties()
                    .group(ItemGroup.FOOD)
                    .food((new Food.Builder())
                            .hunger(1)
                            .saturation(0.1F)
                            .build()
                    )
            )
    );
    public static final RegistryObject<Item> MARGHERITA = ITEMS.register(
            "margherita", () -> new Item(new Item.Properties()
                    .group(ItemGroup.FOOD)
                    .food((new Food.Builder())
                            .hunger(4)
                            .saturation(1.0F)
                            .build()
                    )
            )
    );
    public static final RegistryObject<Item> SUPER_BEEF = ITEMS.register(
            "super_beef", () -> new Item(new Item.Properties()
                    .group(ItemGroup.FOOD)
                    .food((new Food.Builder())
                            .hunger(6)
                            .saturation(3.6F)
                            .build()
                    )
            )
    );
    public static final RegistryObject<Item> SUPER_COOKED_BEEF = ITEMS.register(
            "super_cooked_beef", () -> new Item(new Item.Properties()
                    .group(ItemGroup.FOOD)
                    .food((new Food.Builder())
                            .hunger(16)
                            .saturation(25.6F)
                            .build()
                    )
            )
    );
    public static final RegistryObject<Item> GOLDEN_SUPER_COOKED_BEEF = ITEMS.register(
            "golden_super_cooked_beef", () -> new Item(new Item.Properties()
                    .group(ItemGroup.FOOD)
                    .food((new Food.Builder())
                            .effect(() -> new EffectInstance(Effects.REGENERATION, 100, 2), .5f)
                            .effect(() -> new EffectInstance(Effects.SPEED, 100, 2), .5f)
                            .hunger(32)
                            .saturation(51.2F)
                            .build()
                    )
            )
    );
}
