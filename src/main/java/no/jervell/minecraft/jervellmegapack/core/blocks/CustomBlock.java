package no.jervell.minecraft.jervellmegapack.core.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class CustomBlock extends BaseHorizontalBlock {

    private static final VoxelShape SHAPE = Stream.of(
            Block.makeCuboidShape(11, 7, 3, 12, 8, 10),
            Block.makeCuboidShape(5, 8, 3, 11, 9, 10),
            Block.makeCuboidShape(5, 7, 10, 11, 8, 11),
            Block.makeCuboidShape(4, 7, 3, 5, 8, 10),
            Block.makeCuboidShape(5, 3, 8, 11, 5, 10),
            Block.makeCuboidShape(7, 3, 1, 13, 5, 3),
            Block.makeCuboidShape(4, 5, 7, 6, 7, 13),
            Block.makeCuboidShape(9, 1, 4, 11, 3, 10),
            Block.makeCuboidShape(9, 1, 4, 11, 3, 10),
            Block.makeCuboidShape(3, 1, 6, 5, 3, 12),
            Block.makeCuboidShape(9, 9, 6, 10, 13, 8),
            Block.makeCuboidShape(9, 9, 6, 10, 13, 8),
            Block.makeCuboidShape(6, 9, 6, 7, 13, 8),
            Block.makeCuboidShape(7, 9, 8, 9, 13, 9),
            Block.makeCuboidShape(7, 9, 6, 9, 10, 8),
            Block.makeCuboidShape(7, 9, 5, 9, 13, 6),
            Block.makeCuboidShape(12, 4, 2, 13, 7, 3),
            Block.makeCuboidShape(3, 4, 2, 4, 7, 3),
            Block.makeCuboidShape(6, 8, 2, 10, 9, 3),
            Block.makeCuboidShape(5, 7, 2, 6, 9, 3),
            Block.makeCuboidShape(10, 7, 2, 11, 9, 3),
            Block.makeCuboidShape(4, 6, 2, 5, 8, 3),
            Block.makeCuboidShape(11, 6, 2, 12, 8, 3),
            Block.makeCuboidShape(13, 1, 2, 14, 5, 3),
            Block.makeCuboidShape(6, 7, 2, 10, 8, 3),
            Block.makeCuboidShape(10, 6, 2, 11, 7, 3),
            Block.makeCuboidShape(5, 6, 2, 6, 7, 3),
            Block.makeCuboidShape(4, 4, 2, 5, 6, 3),
            Block.makeCuboidShape(11, 4, 2, 12, 6, 3),
            Block.makeCuboidShape(12, 1, 2, 13, 4, 3),
            Block.makeCuboidShape(3, 1, 2, 4, 4, 3),
            Block.makeCuboidShape(2, 1, 2, 3, 5, 3),
            Block.makeCuboidShape(0, 0, 0, 16, 1, 16),
            Block.makeCuboidShape(4, 5, 10, 6, 7, 11),
            Block.makeCuboidShape(10, 5, 10, 12, 7, 11),
            Block.makeCuboidShape(5, 5, 11, 11, 7, 12),
            Block.makeCuboidShape(12, 5, 7, 13, 7, 10),
            Block.makeCuboidShape(12, 5, 3, 13, 7, 7),
            Block.makeCuboidShape(3, 5, 3, 4, 7, 7),
            Block.makeCuboidShape(13, 3, 3, 14, 5, 10),
            Block.makeCuboidShape(12, 5, 10, 13, 6, 11),
            Block.makeCuboidShape(3, 5, 10, 4, 6, 11),
            Block.makeCuboidShape(3, 5, 7, 4, 7, 10),
            Block.makeCuboidShape(4, 3, 11, 6, 5, 12),
            Block.makeCuboidShape(10, 3, 11, 12, 5, 12),
            Block.makeCuboidShape(5, 3, 12, 11, 5, 13),
            Block.makeCuboidShape(12, 3, 8, 13, 5, 12),
            Block.makeCuboidShape(3, 3, 8, 4, 5, 12),
            Block.makeCuboidShape(2, 3, 3, 3, 5, 10),
            Block.makeCuboidShape(2, 1, 4, 3, 3, 11),
            Block.makeCuboidShape(2, 1, 3, 3, 3, 4),
            Block.makeCuboidShape(13, 1, 3, 14, 3, 4),
            Block.makeCuboidShape(3, 1, 9, 4, 3, 13),
            Block.makeCuboidShape(12, 1, 9, 13, 3, 13),
            Block.makeCuboidShape(5, 1, 13, 11, 3, 14),
            Block.makeCuboidShape(10, 1, 12, 12, 3, 13),
            Block.makeCuboidShape(4, 1, 12, 6, 3, 13),
            Block.makeCuboidShape(13, 1, 4, 14, 3, 11)
    ).reduce((v1, v2) -> {return VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR);}).get();

    public CustomBlock(Properties properties) {
        super(properties);
        runCalculation(SHAPE);
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        return SHAPES.get(state.get(HORIZONTAL_FACING));
    }
}
