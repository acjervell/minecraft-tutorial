package no.jervell.minecraft.jervellmegapack;

import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.RenderTypeLookup;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import no.jervell.minecraft.jervellmegapack.core.init.BlockInit;
import no.jervell.minecraft.jervellmegapack.core.init.ItemInit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(JervellMegaPackMod.MOD_ID)
public class JervellMegaPackMod {
    public static final String MOD_ID = "jervellmegapack";

    // Aleksander ruler!
    private static final Logger LOGGER = LogManager.getLogger();

    public JervellMegaPackMod() {
        IEventBus bus = FMLJavaModLoadingContext.get().getModEventBus();
        bus.addListener(this::setup);

        ItemInit.ITEMS.register(bus);
        BlockInit.BLOCKS.register(bus);

        MinecraftForge.EVENT_BUS.register(this);
    }

    private void setup(final FMLCommonSetupEvent event) {
        RenderTypeLookup.setRenderLayer(BlockInit.TOMATOES.get(), RenderType.getCutout());
    }

}
